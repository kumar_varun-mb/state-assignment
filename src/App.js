import React, { Component } from 'react'

/*
Suppose you are having a data state in React.
Perform the following setState operations

Q1. Change Title of the element to "Hello World" with id = 9 
Q2. Delete all items with userId - 3
Q3. Add a new Element at the end of the array 
    { 
        userId: 2, 
        "id": 11,
        "title": "Quadratic Equations",
        "completed": false
    }

Q4. Add a new Element at position 6
     { 
        userId: 2, 
        "id": 12,
        "title": "Differential Equations",
        "completed": false
    }
Q5. Swap elements at position 3 and 8 .
Q6. Delete element at position 4
Q7. Add a new Array at position 2 
    [{ 
        userId: 2, 
        "id": 13,
        "title": "Kirchoffs Equations",
        "completed": false
    }, { 
        userId: 1, 
        "id": 14,
        "title": "Kepler's Equations",
        "completed": false
    }]
Q8. Delete title Property from element at position 7.
Q9. Swap ids and titles  of elements at positions 2 and 6

*/

export class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: []
    }
  }

  data = [
    {
      "userId": 1,
      "id": 1,
      "title": "delectus aut autem",
      "completed": false
    },
    {
      "userId": 1,
      "id": 2,
      "title": "quis ut nam facilis et officia qui",
      "completed": false
    },
    {
      "userId": 1,
      "id": 3,
      "title": "fugiat veniam minus",
      "completed": false
    },
    {
      "userId": 1,
      "id": 4,
      "title": "et porro tempora",
      "completed": true
    },
    {
      "userId": 3,
      "id": 5,
      "title": "laboriosam mollitia et enim quasi adipisci quia provident illum",
      "completed": false
    },
    {
      "userId": 3,
      "id": 6,
      "title": "qui ullam ratione quibusdam voluptatem quia omnis",
      "completed": false
    },
    {
      "userId": 1,
      "id": 7,
      "title": "illo expedita consequatur quia in",
      "completed": false
    },
    {
      "userId": 1,
      "id": 8,
      "title": "quo adipisci enim quam ut ab",
      "completed": true
    },
    {
      "userId": 1,
      "id": 9,
      "title": "molestiae perspiciatis ipsa",
      "completed": false
    },
    {
      "userId": 3,
      "id": 10,
      "title": "illo est ratione doloremque quia maiores aut",
      "completed": true
    }
  ]

  componentDidMount() {
    this.setState({
      data: this.data
    })
  }

  changeState(question) {
    let newState = this.data.map((obj) => {
      return { ...obj };
    })

    switch (question) {
      case 1:
        newState.map((obj) => {
          if (obj.id == 9)
            obj.title = 'Hello World';
        })
        return newState;

      case 2:
        return this.data.filter((obj) => {
          if (obj.userId == 3)
            return false;
          else
            return true;
        })

      case 3:
        newState.push({
          userId: 2,
          "id": 11,
          "title": "Quadratic Equations",
          "completed": false
        });
        return newState;

      case 4:
        newState.splice(6, 0, {
          userId: 2,
          "id": 12,
          "title": "Differential Equations",
          "completed": false
        })
        return newState;

      case 5:
        let pos3 = { ...newState[3] }
        let pos8 = { ...newState[8] }
        newState[3] = pos8;
        newState[8] = pos3;
        return newState;

      case 6:
        newState.splice(4, 1);
        return newState;
      case 7:
        newState.splice(2, 0, [{
          userId: 2,
          "id": 13,
          "title": "Kirchoffs Equations",
          "completed": false
        }, {
          userId: 1,
          "id": 14,
          "title": "Kepler's Equations",
          "completed": false
        }])
        return newState;
      case 8:
        delete newState[7].title;
        return newState;
      case 9:
        let pos2 = {...newState[2]}
        let pos6 = {...newState[6]}
        newState[2].id= pos6.id;
        newState[2].title = pos6.title;
        newState[6].id= pos2.id;
        newState[6].title = pos2.title;
        return newState;

      default:
        return this.state;
    }
  }

  render() {
    console.log(this.state);
    this.setState(this.changeState(9)); //Enter Question No. in changeState parameter
    return (
      <div>
        <h1>setState Assignment</h1>
      </div>
    )
  }
}

export default App